class Umoci < Formula
  desc "umoci modifies Open Container images"
  homepage "https://umo.ci"
  url "https://github.com/opencontainers/umoci/archive/refs/tags/v0.4.7.tar.gz"
  sha256 "c01b36de6fdc513eb65add57bc882d72f94fc3b4b65a8f9ef59826fb754af93e"
  license "Apache-2.0"

  depends_on "go" => :build
  depends_on "bats-core" => :build
  depends_on "go-md2man" => :build

  def install
    ENV["CGO_ENABLED"] = "1"
    ENV.append "CGO_FLAGS", ENV.cppflags
    ldflag_prefix = "github.com/opencontainers/umoci"
    ldflags = %W[
      -X #{ldflag_prefix}.gitCommit=
      -X #{ldflag_prefix}.version=#{File.open("VERSION").read.chomp}
    ]
    system "go", "build", "-buildvcs=false", *std_go_args(ldflags: ldflags), "./cmd/umoci"
  end

  test do
    system "make", "local-test-integration"
  end
end
